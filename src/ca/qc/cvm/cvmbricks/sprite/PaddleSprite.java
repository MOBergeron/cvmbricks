package ca.qc.cvm.cvmbricks.sprite;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.TextureMng;

public class PaddleSprite extends CVMSprite {
	private static PaddleSprite instance;
	
	private float posX;
	private float posY;
	
	public PaddleSprite() {
		super(CVMGameActivity.CAMERA_WIDTH/2-35, CVMGameActivity.CAMERA_HEIGHT-80, 71, 28, TextureMng.PADDLE_SPRITE);
		posX = super.getInitialX();
		posY = super.getInitialY();
	}
	
	public void restart(){
		posX = super.getInitialX();
		posY = super.getInitialY();
	}
	
	public static PaddleSprite getInstance() {
		if (instance == null) {
			instance = new PaddleSprite();
		}
		
		return instance;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		if(posX + this.getWidth() >= CVMGameActivity.CAMERA_WIDTH){
			this.posX = CVMGameActivity.CAMERA_WIDTH - this.getWidth();
		}
		else if(posX <= 0){
			this.posX = 0;
		}
		else{
			this.posX = posX;
		}
		this.getSprite().setPosition(this.posX, this.posY);
	}
}
