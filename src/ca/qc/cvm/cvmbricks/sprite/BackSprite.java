package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.TextureMng;

public class BackSprite extends CVMSprite implements TouchAreaListener {
	private int sceneid;
	
	public BackSprite(float posX, float posY, int sceneid) {
		super(posX, posY, 69, 31, TextureMng.BACK_SPRITE);
		this.sceneid = sceneid;
	}

	@Override
	public void onAreaTouched(TouchEvent arg0, float arg1, float arg2,
			CVMGameActivity activity, CVMAbstractScene arg4) {
		activity.changeScene(this.sceneid);
		
	}
}
