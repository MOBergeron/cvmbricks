package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.entity.sprite.AnimatedSprite;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.CollisionListener;
import ca.qc.cvm.cvmandengine.entity.ManagedUpdateListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.SoundMng;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.TextureMng;
import ca.qc.cvm.cvmbricks.ParticlesFactory;

public class BallSprite extends CVMSprite implements ManagedUpdateListener, CollisionListener {
	private final int MAX_DISTANCE = 1;
	
	private static BallSprite instance;
	private float posX;
	private float posY;
	private float velX;
	private float velY;
	private int speed = 180;
	private int speedFrame;
	private float elapsedSeconds;
	private boolean starter = false;
	
	public BallSprite() {
		super(CVMGameActivity.CAMERA_WIDTH/2-14, CVMGameActivity.CAMERA_HEIGHT-80-28, 28, 28, TextureMng.BALL_SPRITE);
		posX = super.getInitialX();
		posY = super.getInitialY();
		
		/***************************************************************
		 * Attribuer une valeur entre 0 et 1 � velX
		 * et attribue velY avec Pytagore y^2 = hypoth�nuse^2 - x^2.
		 * Le velY est comme le compl�ment pour arriver � une hypo de 1.
		 * 1^2 = velX^2 + velY^2
		 **************************************************************/
		velX = (float)Math.random();
		velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2));
	}
	
	public void restart(){
		posX = super.getInitialX();
		posY = super.getInitialY();
		velX = (float)Math.random();
		velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2));
		speed = 180;
		speedFrame = 0;
	}
	
	public static BallSprite getInstance() {
		if (instance == null) {
			instance = new BallSprite();
		}
		
		return instance;
	}
	
	public void setStartAnimation(boolean start){
		this.starter = start;
	}
	
	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public float getVelX() {
		return velX;
	}

	public void setVelX(float velX) {
		this.velX = velX;
	}

	public float getVelY() {
		return velY;
	}

	public void setVelY(float velY) {
		this.velY = velY;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpeedFrame() {
		return speedFrame;
	}

	public void setSpeedFrame(int speedFrame) {
		this.speedFrame = speedFrame;
	}

	@Override
	public void managedUpdate(float elapsedSeconds, CVMGameActivity activity, CVMAbstractScene scene) {
		// Commence l'animation.
		if(!starter){
			starter = true;
			AnimatedSprite animatedSprite = (AnimatedSprite)this.getSprite();
			animatedSprite.animate(150,true);
		}
		// Bouge uniquement si l'�tat du jeu est � PLAY
		if(StatementMng.getInstance().getState() == StatementMng.PLAY_STATE){
			float newX, newY;
			this.elapsedSeconds = elapsedSeconds;
			if(velX == 0 || velY == 0){
				if(velX == 0){
					newX = posX;
					newY = posY + velY * speed * elapsedSeconds;
				}
				else{
					newX = posX + velX * speed * elapsedSeconds;
					newY = posY;
				}
			}
			else{
				newX = posX + velX * speed * elapsedSeconds;
				newY = posY + velY * speed * elapsedSeconds;
			}
			
			// Quand on atteind le c�t�, on change de bord
			if(newX < 0){
				velX *= -1;
				posX = 0;
			}
			else if(newX > 800-this.getWidth()){
				velX *= -1;
				posX = 800 - this.getWidth();
			}
			else{
				posX = newX;
			}
			if(newY < 0){
				velY *= -1;
				posY = 0;
			}
			else if(newY > 480-this.getHeight()){
				// Le joueur meurt.
				StatementMng.getInstance().setState(StatementMng.FAIL_STATE);
				activity.stopMusic();
				SoundMng.getInstance().playSound(SoundMng.FAIL);
			}
			else{
				posY = newY;
			}
		}
		this.getSprite().setPosition(posX, posY);
	}

	@Override
	public void collidedWith(CVMGameActivity activity, CVMAbstractScene scene,
			CVMSprite sprite) {
		// Si collision avec la bar, on change de bord.
		if(sprite instanceof PaddleSprite){
			if(posY+this.getHeight()<= PaddleSprite.getInstance().getPosY()+5){
				// La pos2 c'est le point � droite(X) et en bas(Y) de l'objet.
				float posX2, psposX, psposX2;
				posX2 = posX + this.getWidth();
				psposX = PaddleSprite.getInstance().getPosX();
				psposX2 = PaddleSprite.getInstance().getPosX() + PaddleSprite.getInstance().getWidth();
				if(posX - psposX >= psposX2 - posX2-5 && posX - psposX <= psposX2 - posX2+5){
					velX = 0;
					velY = -1;
				}
				else if(posX - psposX >= psposX2 - posX2+10){
					velX = (float)Math.sqrt(0.15);
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2+20){
					velX = (float)Math.sqrt(0.3);
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2+30){
					velX = (float)Math.sqrt(0.5);
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2+40){
					velX = (float)Math.sqrt(0.75);
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2+50){
					velX = (float)Math.sqrt(0.9);
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2-10){
					velX = (float)Math.sqrt(0.2)*-1;
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2-20){
					velX = (float)Math.sqrt(0.15)*-1;
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2-30){
					velX = (float)Math.sqrt(0.5)*-1;
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2-40){
					velX = (float)Math.sqrt(0.75)*-1;
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2-50){
					velX = (float)Math.sqrt(0.9)*-1;
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2-60){
					velX = (float)Math.sqrt(0.9)*-1;
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else if(posX - psposX >= psposX2 - posX2-70){
					velX = (float)Math.sqrt(0.9)*-1;
					velY = (float)Math.sqrt(Math.pow(MAX_DISTANCE, 2)- Math.pow(velX, 2))*-1;
				}
				else{
				}
				posY = CVMGameActivity.CAMERA_HEIGHT-80- this.getHeight();
			}
			StatementMng.getInstance().setCombo(0);
		}
		if(sprite instanceof BrickSprite){
			BrickSprite brick = (BrickSprite)sprite;
			BrickSprite newBrick = null;
			
			float oldX, oldY;
			oldX = posX - velX * speed * elapsedSeconds;
			oldY = posY - velY * speed * elapsedSeconds;
			
			if(!StatementMng.getInstance().isBrickHit()){
				if(oldX + this.getWidth() <= brick.getPosX()){
					velX *= -1;
				}
				else if(oldX >= brick.getPosX() + brick.getWidth()){
					velX *= -1;
				}
				else if(oldY + this.getHeight() <= brick.getPosY()){
					velY *= -1;
				}
				else if(oldY >= brick.getPosY() + brick.getHeight()){
					velY *= -1;
				}
			}
			
			StatementMng.getInstance().setCombo(StatementMng.getInstance().getCombo()+1);
			StatementMng.getInstance().setScore(StatementMng.getInstance().getScore()+(10*StatementMng.getInstance().getCombo()));
			StatementMng.getInstance().setBrickHit(true);
			
			ParticlesFactory.addExplosion(brick.getPosX()+16, brick.getPosY()+16, scene);
			
			newBrick = brick.die(scene);
			StatementMng.getInstance().removeBrick(brick);
			if(newBrick != null){
				StatementMng.getInstance().addNewBrick(newBrick);
			}
		}
	}
}
