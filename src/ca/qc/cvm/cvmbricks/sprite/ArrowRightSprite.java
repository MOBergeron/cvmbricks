package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.TextureMng;

public class ArrowRightSprite extends CVMSprite implements TouchAreaListener{
	
	public ArrowRightSprite() {
		super(CVMGameActivity.CAMERA_WIDTH-100, CVMGameActivity.CAMERA_HEIGHT-50, 100, 50, TextureMng.ARROW_RIGHT_SPRITE);
	}


	@Override
	public void onAreaTouched(TouchEvent touch, float x, float y,
			CVMGameActivity activity, CVMAbstractScene scene) {
		if(StatementMng.getInstance().getState() == StatementMng.PLAY_STATE){
			StatementMng.getInstance().setMoveRight(true);
		}
		if(touch.getAction()==TouchEvent.ACTION_UP){
			StatementMng.getInstance().setMoveRight(false);
		}
	}
}
