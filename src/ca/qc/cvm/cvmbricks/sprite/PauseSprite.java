package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.TextureMng;

public class PauseSprite extends CVMSprite implements TouchAreaListener{
	
	public PauseSprite() {
		super(CVMGameActivity.CAMERA_WIDTH-50, 5, 25, 30, TextureMng.PAUSE_SPRITE);
	}

	@Override
	public void onAreaTouched(TouchEvent touch, float x, float y,
			CVMGameActivity activity, CVMAbstractScene scene) {
		if(touch.getAction() == TouchEvent.ACTION_UP){
			if(StatementMng.getInstance().getState() != StatementMng.PAUSE_STATE){
				StatementMng.getInstance().setState(StatementMng.PAUSE_STATE);
			}
			else
			{
				StatementMng.getInstance().setState(StatementMng.WAITING_STATE);
			}
		}
	}
}
