package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.TextureMng;

public class HighscoreSprite extends CVMSprite implements TouchAreaListener {
	
	public HighscoreSprite() {
		super((CVMGameActivity.CAMERA_WIDTH-256)/2, 260, 256, 54, TextureMng.HIGHSCORE_SPRITE);
	}

	@Override
	public void onAreaTouched(TouchEvent arg0, float arg1, float arg2,
			CVMGameActivity activity, CVMAbstractScene arg4) {
		activity.changeScene(5, true);
		
	}
}
