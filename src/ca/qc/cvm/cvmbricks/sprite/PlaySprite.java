package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.TextureMng;

public class PlaySprite extends CVMSprite implements TouchAreaListener {
	
	public PlaySprite() {
		super((CVMGameActivity.CAMERA_WIDTH-121)/2, 190, 121, 54, TextureMng.PLAY_SPRITE);
	}

	@Override
	public void onAreaTouched(TouchEvent arg0, float arg1, float arg2,
			CVMGameActivity activity, CVMAbstractScene scene) {
		activity.changeScene(4, true);
	}
}
