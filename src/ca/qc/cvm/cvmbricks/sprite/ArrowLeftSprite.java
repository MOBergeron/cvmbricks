package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.TextureMng;

public class ArrowLeftSprite extends CVMSprite implements TouchAreaListener{
	
	public ArrowLeftSprite() {
		super(0, CVMGameActivity.CAMERA_HEIGHT-50, 100, 50, TextureMng.ARROW_LEFT_SPRITE);
	}


	@Override
	public void onAreaTouched(TouchEvent touch, float x, float y,
			CVMGameActivity activity, CVMAbstractScene scene) {
		if(StatementMng.getInstance().getState() == StatementMng.PLAY_STATE){
			StatementMng.getInstance().setMoveLeft(true);
		}
		if(touch.getAction()==TouchEvent.ACTION_UP){
			StatementMng.getInstance().setMoveLeft(false);
		}
	}
}
