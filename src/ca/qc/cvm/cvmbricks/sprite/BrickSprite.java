package ca.qc.cvm.cvmbricks.sprite;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmbricks.StatementMng;

public class BrickSprite extends CVMSprite {
	
	private float posX;
	private float posY;
	private int brickLevel;
	
	public BrickSprite(float x, float y, int textureId) {
		super(x, y, 64, 64, textureId);
		posX = super.getInitialX();
		posY = super.getInitialY();
		switch(textureId){
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:	brickLevel = 1;
			break;
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:	brickLevel = 2;
			break;
		case 20:
		case 21:
		case 22:
		case 23:
		case 24:
		case 25:	brickLevel = 3;
			break;
		case 26:
		case 27:
		case 28:
		case 29:
		case 30:
		case 31:	brickLevel = 4;
			break;
		case 32:
		case 33:
		case 34:
		case 35:
		case 36:
		case 37:	brickLevel = 5;
			break;
		case 38:
		case 39:
		case 40:
		case 41:
		case 42:
		case 43:	brickLevel = 6;
			break;
		}
	}
	
	public BrickSprite die(CVMAbstractScene scene){
		scene.removeSprite(this);
		if(brickLevel == 6){
			BallSprite.getInstance().setSpeed(220);
			BallSprite.getInstance().setSpeedFrame(StatementMng.getInstance().getFrame());
		}
		if(brickLevel >= 2){
			return new BrickSprite(this.posX, this.posY, this.getTextureId()-6);
		}
		else{
			return null;
		}
	}
	
	public int getBrickLevel() {
		return brickLevel;
	}

	public void setBrickLevel(int brickLevel) {
		this.brickLevel = brickLevel;
	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}
}
