package ca.qc.cvm.cvmbricks.sprite;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.entity.TouchAreaListener;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.StatementMng;

public class LevelSprite extends CVMSprite implements TouchAreaListener{
	
	private float posX;
	private float posY;
	private int level;
	private int sceneid;
	
	public LevelSprite(float x, float y, int textureId, int level, int sceneid) {
		super(x, y, 64, 64, textureId);
		posX = super.getInitialX();
		posY = super.getInitialY();
		this.level = level;
		this.sceneid = sceneid;
	}
	
	public void restart(){
		posX = super.getInitialX();
		posY = super.getInitialY();
	}
	
	public void die(CVMAbstractScene scene){
		scene.removeSprite(this);
	}
	
	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	@Override
	public void onAreaTouched(TouchEvent touch, float x, float y,
			CVMGameActivity activity, CVMAbstractScene scene) {
		
		if(StatementMng.getInstance().getState() == StatementMng.MENU_STATE 
				&& StatementMng.getInstance().getUserLevel() >= level){
			if(touch.getAction() == TouchEvent.ACTION_UP){
				StatementMng.getInstance().setLevel(level);
				activity.changeScene(sceneid, true);
			}
		}
	}
}
