package ca.qc.cvm.cvmbricks.sprite;

import ca.qc.cvm.cvmandengine.entity.CVMSprite;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.TextureMng;

public class TitleSprite extends CVMSprite {
	
	public TitleSprite() {
		super((CVMGameActivity.CAMERA_WIDTH-526)/2, 50, 526, 94, TextureMng.TITLE_SPRITE);
	}
}
