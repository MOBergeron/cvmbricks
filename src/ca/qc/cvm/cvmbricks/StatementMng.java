package ca.qc.cvm.cvmbricks;

import java.util.ArrayList;
import java.util.List;

import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.sprite.BrickSprite;

public class StatementMng {
	public static final int  PLAY_STATE = 0;
	public static final int  PAUSE_STATE = 1;
	public static final int  WAITING_STATE = 2;
	public static final int  MENU_STATE = 3;
	public static final int  WIN_STATE = 4;
	public static final int  FAIL_STATE = 5;
	public static final int  LEFT_MOVE = 100;
	public static final int  RIGHT_MOVE = 101;
	
	private int combo;
	private int score;
	private int level;
	private int userLevel;
	private int state;
	private int frame;
	private boolean[] movement;
	private boolean brickHit;
	
	private List<BrickSprite> bricklist;
	private List<BrickSprite> newBricklist;
	
	private CVMAbstractScene scene;
	
	private static StatementMng instance;
	
	private StatementMng() {
		// Cr�ation des ressources
		bricklist = new ArrayList<BrickSprite>();
		newBricklist = new ArrayList<BrickSprite>();
		state = MENU_STATE;
		movement = new boolean[2];
		movement[0] = false;
		movement[1] = false;
	}
	
	public void addNewBrick(BrickSprite brick){
		this.newBricklist.add(brick);
	}
	
	public void addBrick(BrickSprite brick){
		this.bricklist.add(brick);
	}
	
	public void removeBrick(BrickSprite brick){
		this.bricklist.remove(brick);
	}
	
	public void clearNewBrickList(){
		this.newBricklist.clear();
	}
	
	public List<BrickSprite> updateBrickLists(){
		if(!this.newBricklist.isEmpty()){
			for(BrickSprite i : this.newBricklist){
				this.bricklist.add(i);
			}
			return this.newBricklist;
		}
		return null;
	}
	
	public CVMAbstractScene getScene() {
		return scene;
	}

	public void setScene(CVMAbstractScene scene) {
		this.scene = scene;
	}

	public List<BrickSprite> getBricklist() {
		return bricklist;
	}

	public void setBricklist(List<BrickSprite> bricklist) {
		this.bricklist = bricklist;
	}
	
	public void setMoveLeft(boolean move){
		this.movement[0] = move;
	}
	
	public void setMoveRight(boolean move){
		this.movement[1] = move;
	}
	
	public boolean getMovement(int move){
		if(move == LEFT_MOVE){
			return this.movement[0];
		}
		if(move == RIGHT_MOVE){
			return this.movement[1];
		}
		return false;
	}

	public int getFrame() {
		return frame;
	}

	public void setFrame(int frame) {
		this.frame = frame;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getCombo() {
		return combo;
	}

	public void setCombo(int combo) {
		this.combo = combo;
	}

	public boolean isBrickHit() {
		return brickHit;
	}

	public void setBrickHit(boolean brickHit) {
		this.brickHit = brickHit;
	}

	public static StatementMng getInstance() {
		if (instance == null) {
			instance = new StatementMng();
		}
		
		return instance;
	}
}