package ca.qc.cvm.cvmbricks;

import java.util.ArrayList;
import java.util.List;

import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.SoundMng;
import ca.qc.cvm.cvmbricks.TextureMng;
import ca.qc.cvm.cvmbricks.scene.GameScene;
import ca.qc.cvm.cvmbricks.scene.HighscoreChoosingScene;
import ca.qc.cvm.cvmbricks.scene.HighscoreScene;
import ca.qc.cvm.cvmbricks.scene.LevelScene;
import ca.qc.cvm.cvmbricks.scene.SplashScene;

public class MainActivity extends CVMGameActivity {

	public MainActivity() {
		super(TextureMng.getInstance());
		
		// Assignation du SoundMng, permettant de jouer des bruits, comme des explosions
		super.setSoundManager(SoundMng.getInstance());
		
		// Cr�ation des sc�nes et ajout de ces sc�nes � l'activit�
		List<CVMAbstractScene> sceneList = new ArrayList<CVMAbstractScene>();
		sceneList.add(new SplashScene());
		sceneList.add(new GameScene());
		sceneList.add(new HighscoreScene());
		sceneList.add(new HighscoreChoosingScene());
		sceneList.add(new LevelScene());
		
		super.setSceneList(sceneList);
	}

}
