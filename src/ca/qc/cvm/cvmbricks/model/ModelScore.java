package ca.qc.cvm.cvmbricks.model;

import java.util.ArrayList;
import java.util.List;

import ca.qc.cvm.cvmbricks.entity.Level;
import ca.qc.cvm.cvmbricks.entity.Score;
import ca.qc.cvm.cvmbricks.model.db.DatabaseHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ModelScore {
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase database;

	public ModelScore(Context ctx) {
		databaseHelper = new DatabaseHelper(ctx);
	}
	
	public void openDatabase() {
		database = databaseHelper.getWritableDatabase();
	}
	
	public void closeDatabase() {
		database.close();
	}
	
	public long nextLevel(Level level){
		ContentValues values = new ContentValues();
		values.put("level", level.getLevel()+1);
		return database.update("user", values,"id = 1",null);
	}
	
	public long addScore(Score score){
		//on ne met pas l'id parce qu'il AUTOINCREMENT
		ContentValues values = new ContentValues();
		values.put("level", score.getLevel());
		values.put("score", score.getScore());
		return database.insert("score", null, values);
	}
	
	public void deleteScore(long line){
		database.delete("score","id = " + line, null);
	}
	
	public List<Score> fetchScores(){
		List<Score> scoreList = new ArrayList<Score>();
		Cursor cursor = database.query(true, "score", new String[]{"id", "level" , "score"},
				null, null, null, null, "score DESC", null, null);
		
		if (cursor != null && cursor.moveToFirst()){
			do{
				scoreList.add(new Score(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2)));
			}while(cursor.moveToNext()); // Aller au prochain si != null
		}
		
		return scoreList;
	}
	
	public Score getScore(long id){
		Cursor cursor = database.query(true, "score", new String[]{"id", "level" , "score"},
				"id = " + id, null, null, null, null, null, null);
		
		Score score = null;
		
		if (cursor != null){
			cursor.moveToFirst();
			
		}
		
		return score;
	}
	
	public List<Level> fetchLevels(){
		List<Level> levelList = new ArrayList<Level>();
		Cursor cursor = database.query(true, "user", new String[]{"id", "level"},
				null, null, null, null, null, null, null);
		
		if (cursor != null && cursor.moveToFirst()){
			do{
				levelList.add(new Level(cursor.getInt(0), cursor.getInt(1)));
			}while(cursor.moveToNext()); // Aller au prochain si != null
		}
		
		return levelList;
	}
	
	public long getLevel(long id){
		Cursor cursor = database.query(true, "user", new String[]{"id", "level"},
				"id = " + id, null, null, null, null, null, null);
		
		int level = 0;
		
		if (cursor != null && cursor.moveToFirst()){
			level = cursor.getInt(1);
		}
		
		return level;
	}
}
