package ca.qc.cvm.cvmbricks.model.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATBASE_NAME = "db_score";
	private static final int DATABASE_VERSION = 1;
	
	public DatabaseHelper(Context context) {
		super(context, DATBASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			//Cr�ation des tables
			db.execSQL("CREATE TABLE score (" +
					"  id        INTEGER PRIMARY KEY AUTOINCREMENT," +
					"  level	 INTEGER, " +
					"  score     INTEGER" +
					")");
			
			db.execSQL("CREATE TABLE user	(" +
					"  id		INTEGER PRIMARY KEY AUTOINCREMENT," +
					"  level	INTEGER" +
					")");
			
			db.execSQL("INSERT INTO user VALUES (1,1)");
			Log.i("CVMSQLite", "BD cr��e");
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS score;" +
				"DROP TABLE IF EXISTS user;");
		onCreate(db);
	}
}
