package ca.qc.cvm.cvmbricks.scene;

import java.util.List;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmbricks.BackgroundMng;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.entity.Score;
import ca.qc.cvm.cvmbricks.model.ModelScore;

public class HighscoreScene extends CVMAbstractScene {

	public HighscoreScene() {
		super(BackgroundMng.getInstance().getRandomBackground(), 3);
	}

	@Override
	public void sceneTouched(TouchEvent touch) {
	}

	@Override
	public void managedUpdate(float arg0) {
	}

	@Override
	public void starting() {
		StatementMng.getInstance().setState(StatementMng.MENU_STATE);
		
		// Choisi la musique
		this.gameActivity.setMusic("music/pirate1.mp3");
		
		ModelScore modelScore = new ModelScore(this.gameActivity);
		modelScore.openDatabase();
		CVMText hsText;
		int y = 20, x = 20;
    	List<Score> scoreList = modelScore.fetchScores();
    	for (Score score: scoreList){
    		if(score.getLevel() == StatementMng.getInstance().getLevel()){
	    		hsText = new CVMText(x, y, 40, String.valueOf(score.getLevel()) + ": " + String.valueOf(score.getScore()), Color.WHITE);
	    		this.addText(hsText);
	    		
	    		if(y == 420){
	    			x+=260;
	    			y = 20;
	    		}
	    		else{
	    			y+=50;
	    		}
    		}
    	}
    	
    	modelScore.closeDatabase();
	}
}
