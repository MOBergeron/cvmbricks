package ca.qc.cvm.cvmbricks.scene;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmbricks.BackgroundMng;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.TextureMng;
import ca.qc.cvm.cvmbricks.sprite.LevelSprite;
import ca.qc.cvm.cvmbricks.sprite.TitleSprite;

public class HighscoreChoosingScene extends CVMAbstractScene {

	private float waitingTime;
	
	public HighscoreChoosingScene() {
		super(BackgroundMng.getInstance().getRandomBackground(), 5);	
		super.addSprite(new TitleSprite());
	}

	@Override
	public void sceneTouched(TouchEvent touch) {
	}

	@Override
	public void managedUpdate(float elapsedseconds) {
		if(StatementMng.getInstance().getState() == StatementMng.WAITING_STATE){
			waitingTime += elapsedseconds;
			if(waitingTime >= 0.2){
				StatementMng.getInstance().setState(StatementMng.MENU_STATE);
			}
		}
	}

	@Override
	public void starting() {
		StatementMng.getInstance().setState(StatementMng.MENU_STATE);
		
		// Choisi la musique
		this.gameActivity.setMusic("music/pirate1.mp3");
		
		for(int x=176, y=200, i = 0; i < 8; x+=128, i++){
			this.addSprite(new LevelSprite(x,y,TextureMng.BRICK_YELLOW_SPRITE2, i+1, 3));
			if(x == 560){
				x = 48;
				y = 350;
			}
		}
		
		for(int x=200, y=214, i = 0; i < 8; x+=128, i++){
			if(StatementMng.getInstance().getUserLevel() >= i+1){
				super.addText(new CVMText(x,y,30,String.valueOf(i+1), Color.WHITE));
			}
			else{
				super.addText(new CVMText(x,y,30,String.valueOf(i+1), Color.BLACK));
			}
			if(x == 584){
				x = 72;
				y = 364;
			}
		}
	}
}
