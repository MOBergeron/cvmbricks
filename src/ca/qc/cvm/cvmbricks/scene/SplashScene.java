package ca.qc.cvm.cvmbricks.scene;

import org.andengine.input.touch.TouchEvent;

import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmbricks.BackgroundMng;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.model.ModelScore;
import ca.qc.cvm.cvmbricks.sprite.HighscoreSprite;
import ca.qc.cvm.cvmbricks.sprite.PlaySprite;
import ca.qc.cvm.cvmbricks.sprite.TitleSprite;

public class SplashScene extends CVMAbstractScene {

	public SplashScene() {
		super(BackgroundMng.getInstance().getBackground(0), 1);
		
		super.addSprite(new TitleSprite());
		super.addSprite(new PlaySprite());
		super.addSprite(new HighscoreSprite());
	}

	@Override
	public void sceneTouched(TouchEvent touch) {
	}

	@Override
	public void managedUpdate(float arg0) {
	}

	@Override
	public void starting() {
		StatementMng.getInstance().setState(StatementMng.MENU_STATE);
		
		// Choisi la musique
		this.gameActivity.setMusic("music/pirate1.mp3");
		
		super.addSprite(new TitleSprite());
		super.addSprite(new PlaySprite());
		super.addSprite(new HighscoreSprite());
		
		//Va chercher le level de l'utilisateur
		ModelScore modelScore = new ModelScore(this.gameActivity);
		modelScore.openDatabase();
		
    	StatementMng.getInstance().setUserLevel((int)modelScore.getLevel(1));
    	
    	modelScore.closeDatabase();
	}
}
