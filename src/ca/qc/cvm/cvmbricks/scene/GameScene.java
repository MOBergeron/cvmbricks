package ca.qc.cvm.cvmbricks.scene;

import java.util.List;

import org.andengine.input.touch.TouchEvent;
import org.andengine.util.color.Color;

import ca.qc.cvm.cvmandengine.entity.CVMText;
import ca.qc.cvm.cvmandengine.scene.CVMAbstractScene;
import ca.qc.cvm.cvmandengine.ui.CVMGameActivity;
import ca.qc.cvm.cvmbricks.BackgroundMng;
import ca.qc.cvm.cvmbricks.StatementMng;
import ca.qc.cvm.cvmbricks.TextureMng;
import ca.qc.cvm.cvmbricks.entity.Level;
import ca.qc.cvm.cvmbricks.entity.Score;
import ca.qc.cvm.cvmbricks.model.ModelScore;
import ca.qc.cvm.cvmbricks.sprite.ArrowLeftSprite;
import ca.qc.cvm.cvmbricks.sprite.ArrowRightSprite;
import ca.qc.cvm.cvmbricks.sprite.BallSprite;
import ca.qc.cvm.cvmbricks.sprite.BrickSprite;
import ca.qc.cvm.cvmbricks.sprite.PaddleSprite;
import ca.qc.cvm.cvmbricks.sprite.PauseSprite;
import ca.qc.cvm.cvmbricks.SoundMng;

public class GameScene extends CVMAbstractScene {
	
	private CVMText waitingText;
	private CVMText scoreText;
	private CVMText comboText;
	private CVMText pauseText;
	private CVMText failText;
	private CVMText winText;
	private float waitingTime;
	private boolean pause;
	private boolean isWaiting;
	
	public GameScene() {
		super(BackgroundMng.getInstance().getRandomBackground(), 2);
		
		super.addSprite(PaddleSprite.getInstance());
		super.addSprite(BallSprite.getInstance());
		super.addSprite(new PauseSprite());
		super.addSprite(new ArrowLeftSprite());
		super.addSprite(new ArrowRightSprite());
		
		// Textes affich�s � l'�cran �ventuellement.
		pauseText = new CVMText(270, 200, 90, "PAUSE", Color.WHITE);
		failText = new CVMText(270, 200, 90,"FAILED", Color.WHITE);
		winText = new CVMText(200, 200, 90,"GOOD GAME!", Color.WHITE);
	}

	@Override
	public void sceneTouched(TouchEvent touch) {
		if(StatementMng.getInstance().getState() == StatementMng.WIN_STATE || StatementMng.getInstance().getState() == StatementMng.FAIL_STATE){
			ModelScore modelScore = new ModelScore(this.gameActivity);
			modelScore.openDatabase();
			
			if(StatementMng.getInstance().getScore() > 0){
				modelScore.addScore(new Score(0, StatementMng.getInstance().getLevel(), StatementMng.getInstance().getScore()));
			}
        	
        	if(StatementMng.getInstance().getState() == StatementMng.WIN_STATE && StatementMng.getInstance().getUserLevel() == StatementMng.getInstance().getLevel()){
        		modelScore.nextLevel(new Level(0, StatementMng.getInstance().getUserLevel()));
    			StatementMng.getInstance().setUserLevel((StatementMng.getInstance().getUserLevel()+1));
            	super.removeText(winText);
        	}
        	else{
            	super.removeText(failText);
        	}
        	
        	if(isWaiting){
        		super.removeText(waitingText);
        	}
        	
        	super.removeText(scoreText);
        	super.removeText(comboText);
        	
        	modelScore.closeDatabase();
        	
			this.gameActivity.onBackPressed();
		}
	}

	@Override
	public void managedUpdate(float elapsedseconds) {
		if(StatementMng.getInstance().getState() == StatementMng.WAITING_STATE){
			this.waitingStatement(elapsedseconds);
		}
		else if(StatementMng.getInstance().getState()== StatementMng.PLAY_STATE){
			this.playStatement();
		}
		else if(StatementMng.getInstance().getState() == StatementMng.PAUSE_STATE){
			this.pauseStatement();
		}
		else if(StatementMng.getInstance().getState() == StatementMng.WIN_STATE){
			this.winStatement();
		}
		else if(StatementMng.getInstance().getState() == StatementMng.FAIL_STATE){
			this.failStatement();
		}
	}
	/*******************************************************
	 * Les valeurs en Y peuvent �tre : 
	 * 50, 114, 178, 242, 306.
	 * Les valeurs en X peuvent �tre : 
	 * 36, 100, 164, 228, 292, 256, 420, 484, 548, 612, 676
	 *******************************************************/

	@Override
	public void starting() {
		// Choisi la musique
		this.gameActivity.setMusic("music/pirate2.mp3");
		
		// Prend le niveau et cr�e les briques appropri�es.
		this.setGameLevel();
		
		// Sp�cialement pour le niveau 7, on commence au milieu.
		if(StatementMng.getInstance().getLevel() == 7){
			BallSprite.getInstance().restart();
			PaddleSprite.getInstance().restart();
			BallSprite.getInstance().setPosY(CVMGameActivity.CAMERA_HEIGHT/2);
		}
		else{
			BallSprite.getInstance().restart();
			PaddleSprite.getInstance().restart();
		}
		// Rajout des briques � la sc�ne.
		for(BrickSprite i : StatementMng.getInstance().getBricklist()){
			super.addSprite(i);
		}
		
		// Met le jeu en attente (countdown).
		StatementMng.getInstance().setState(StatementMng.WAITING_STATE);
		
		// Remet certaines variables � z�ro.
		StatementMng.getInstance().setFrame(0);
		StatementMng.getInstance().setScore(0);
		StatementMng.getInstance().setCombo(0);
		
		pause = false;
		isWaiting = false;
		
		waitingTime = 0;
		
		scoreText = new CVMText(5,5,30,"Score: " + String.valueOf(StatementMng.getInstance().getScore()) + "         ",Color.WHITE);
		comboText = new CVMText(575,5,30,"Combo: " + String.valueOf(StatementMng.getInstance().getCombo()) + "  ",Color.WHITE);
		waitingText = new CVMText(300, 214, 70," 3! ", Color.WHITE);
		super.addText(scoreText);
		super.addText(comboText);
		super.addText(waitingText);
	}
	
	public void waitingStatement(float elapsedseconds){
		// Countdown avant de jouer (3..., 2..., 1..., GO...)
		waitingTime += elapsedseconds;
		
		if((waitingTime >= 0 && waitingTime < 1) ){
			if(pause){
				waitingText = new CVMText(300, 214, 70," 3! ", Color.WHITE);
				
				super.addText(waitingText);
				super.removeText(pauseText);
				
				pause = false;
			}
			isWaiting = true;
		}
		else if((waitingTime >= 1 && waitingTime < 2) ){
			waitingText.getText().setText(" 2! ");
		}
		else if((waitingTime >= 2 && waitingTime < 3) ){
			waitingText.getText().setText(" 1! ");
		}
		else if((waitingTime >= 3 && waitingTime < 4) ){
			waitingText.getText().setText("GO!");
		}
		else if(waitingTime >= 4){
			StatementMng.getInstance().setState(StatementMng.PLAY_STATE);
			super.removeText(waitingText);
			isWaiting = false;
		}
	}
	
	public void playStatement(){
		//Actualise le texte du score
		scoreText.getText().setText("Score: " + String.valueOf(StatementMng.getInstance().getScore()));
		comboText.getText().setText("Combo: " + String.valueOf(StatementMng.getInstance().getCombo()));
		
		// Update les briques. Rajouts des nouvelles briques.
		List<BrickSprite> list = StatementMng.getInstance().updateBrickLists();
		if(list != null){
			for(BrickSprite i : list){
				super.addSprite(i);	
			}
			StatementMng.getInstance().clearNewBrickList();
		}
		
		// Si les fl�ches sont enfonc�es, le paddle bouge.
		if(StatementMng.getInstance().getMovement(StatementMng.LEFT_MOVE)){
			PaddleSprite.getInstance().setPosX(PaddleSprite.getInstance().getPosX()-5);
		}
		
		if(StatementMng.getInstance().getMovement(StatementMng.RIGHT_MOVE)){
			PaddleSprite.getInstance().setPosX(PaddleSprite.getInstance().getPosX()+5);
		}
		
		// Si la liste de brique est vide, on gagne.
		if(StatementMng.getInstance().getBricklist().isEmpty()){
			StatementMng.getInstance().setState(StatementMng.WIN_STATE);
			this.gameActivity.stopMusic();
			SoundMng.getInstance().playSound(SoundMng.GG);
		}
		
		// Si on touche une brique niveau 6, on augmente la vitesse sur 100 frames.
		if(BallSprite.getInstance().getSpeedFrame()+100 == StatementMng.getInstance().getFrame()){
			BallSprite.getInstance().setSpeed(180);
		}
		
		// Augmente des frames
		StatementMng.getInstance().setFrame(StatementMng.getInstance().getFrame()+1);
		
		StatementMng.getInstance().setBrickHit(false);
	}
	
	public void pauseStatement(){
		if(!pause){
			pause = true;
			
			super.addText(pauseText);
			
			waitingTime = 0;
			
			if(isWaiting){
				super.removeText(waitingText);
				
				isWaiting = false;
			}
		}
	}
	
	public void failStatement(){
		super.addText(failText);
	}
	
	public void winStatement(){
		super.addText(winText);
	}
	
	public void setGameLevel(){
		// Tue tous les briques (enl�ve de la sc�ne).
		for(BrickSprite i : StatementMng.getInstance().getBricklist()){
			i.die(this);
		}
		// Vide la liste de brique.
		StatementMng.getInstance().getBricklist().clear();
		
		if(StatementMng.getInstance().getLevel() == 1){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(36, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 50, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(36, 114, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 114, TextureMng.getInstance().getRandomBrickWithLvl(1))));
		}
		else if(StatementMng.getInstance().getLevel() == 2){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(36, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(36, 114, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 114, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			
			// Rang�e 3
			StatementMng.getInstance().addBrick((new BrickSprite(36, 178, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 178, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			
			// Rang�e 4
			StatementMng.getInstance().addBrick((new BrickSprite(36, 242, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 242, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			
			// Rang�e 5
		}
		else if(StatementMng.getInstance().getLevel() == 3){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(36, 50, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 50, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(36, 114, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 114, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			
			// Rang�e 3
			StatementMng.getInstance().addBrick((new BrickSprite(36, 178, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 178, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			
			// Rang�e 4
			StatementMng.getInstance().addBrick((new BrickSprite(36, 242, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 242, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			
			// Rang�e 5
		}
		else if(StatementMng.getInstance().getLevel() == 4){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(36, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(484, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			// Rang�e 3
			StatementMng.getInstance().addBrick((new BrickSprite(676, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			// Rang�e 4
			StatementMng.getInstance().addBrick((new BrickSprite(164, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			// Rang�e 5
			StatementMng.getInstance().addBrick((new BrickSprite(612, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
		}
		else if(StatementMng.getInstance().getLevel() == 5){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(36, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(36, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 3
			StatementMng.getInstance().addBrick((new BrickSprite(36, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 4
			StatementMng.getInstance().addBrick((new BrickSprite(36, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 5
			StatementMng.getInstance().addBrick((new BrickSprite(36, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
		}
		else if(StatementMng.getInstance().getLevel() == 6){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(36, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 50, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 50, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 50, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 50, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 50, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(36, 114, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 114, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 114, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 114, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 114, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 114, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			
			// Rang�e 3
			StatementMng.getInstance().addBrick((new BrickSprite(36, 178, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 178, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 178, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 178, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			
			// Rang�e 4
			StatementMng.getInstance().addBrick((new BrickSprite(36, 242, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 242, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 242, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 242, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			
			// Rang�e 5
			StatementMng.getInstance().addBrick((new BrickSprite(36, 306, TextureMng.getInstance().getRandomBrickWithLvl(3))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 306, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 306, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 306, TextureMng.getInstance().getRandomBrickWithLvl(1))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 306, TextureMng.getInstance().getRandomBrickWithLvl(2))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 306, TextureMng.getInstance().getRandomBrickWithLvl(3))));
		}
		else if(StatementMng.getInstance().getLevel() == 7){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(36, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(36, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 3
			StatementMng.getInstance().addBrick((new BrickSprite(36, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 4
			StatementMng.getInstance().addBrick((new BrickSprite(36, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 5
			StatementMng.getInstance().addBrick((new BrickSprite(36, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 306, TextureMng.getInstance().getRandomBrickWithLvl(4))));
		}
		else if(StatementMng.getInstance().getLevel() == 8){
			// Rang�e 1
			StatementMng.getInstance().addBrick((new BrickSprite(100, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 50, TextureMng.getInstance().getRandomBrickWithLvl(5))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 50, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 50, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 50, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 50, TextureMng.getInstance().getRandomBrickWithLvl(5))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 50, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 2
			StatementMng.getInstance().addBrick((new BrickSprite(36, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(100, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 114, TextureMng.getInstance().getRandomBrickWithLvl(5))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 114, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 114, TextureMng.getInstance().getRandomBrickWithLvl(5))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 114, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 3
			StatementMng.getInstance().addBrick((new BrickSprite(100, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(164, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 178, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(612, 178, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 4
			StatementMng.getInstance().addBrick((new BrickSprite(164, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(228, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(292, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(356, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(420, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(484, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			StatementMng.getInstance().addBrick((new BrickSprite(548, 242, TextureMng.getInstance().getRandomBrickWithLvl(4))));
			
			// Rang�e 5
			StatementMng.getInstance().addBrick((new BrickSprite(36, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
			StatementMng.getInstance().addBrick((new BrickSprite(676, 306, TextureMng.getInstance().getRandomBrickWithLvl(6))));
		}
	}
}
