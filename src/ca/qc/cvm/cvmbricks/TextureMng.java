package ca.qc.cvm.cvmbricks;

import java.util.Random;

import ca.qc.cvm.cvmandengine.CVMTextureManager;
import ca.qc.cvm.cvmandengine.entity.CVMTexture;
import ca.qc.cvm.cvmbricks.TextureMng;

public class TextureMng extends CVMTextureManager {
	public static final int PADDLE_SPRITE = 1;
	public static final int BALL_SPRITE = 2;
	public static final int TITLE_SPRITE = 3;
	public static final int PLAY_SPRITE = 4;
	public static final int HIGHSCORE_SPRITE = 5;
	public static final int PAUSE_SPRITE = 6;
	public static final int BACK_SPRITE = 7;
	public static final int BRICK_ORANGE_SPRITE1 = 8;
	public static final int BRICK_ORANGE_SPRITE2 = 14;
	public static final int BRICK_ORANGE_SPRITE3 = 20;
	public static final int BRICK_ORANGE_SPRITE4 = 26;
	public static final int BRICK_ORANGE_SPRITE5 = 32;
	public static final int BRICK_ORANGE_SPRITE6 = 38;
	public static final int BRICK_PINK_SPRITE1 = 9;
	public static final int BRICK_PINK_SPRITE2 = 15;
	public static final int BRICK_PINK_SPRITE3 = 21;
	public static final int BRICK_PINK_SPRITE4 = 27;
	public static final int BRICK_PINK_SPRITE5 = 33;
	public static final int BRICK_PINK_SPRITE6 = 39;
	public static final int BRICK_RED_SPRITE1 = 10;
	public static final int BRICK_RED_SPRITE2 = 16;
	public static final int BRICK_RED_SPRITE3 = 22;
	public static final int BRICK_RED_SPRITE4 = 28;
	public static final int BRICK_RED_SPRITE5 = 34;
	public static final int BRICK_RED_SPRITE6 = 40;
	public static final int BRICK_BLUE_SPRITE1 = 11;
	public static final int BRICK_BLUE_SPRITE2 = 17;
	public static final int BRICK_BLUE_SPRITE3 = 23;
	public static final int BRICK_BLUE_SPRITE4 = 29;
	public static final int BRICK_BLUE_SPRITE5 = 35;
	public static final int BRICK_BLUE_SPRITE6 = 41;
	public static final int BRICK_GREEN_SPRITE1 = 12;
	public static final int BRICK_GREEN_SPRITE2 = 18;
	public static final int BRICK_GREEN_SPRITE3 = 24;
	public static final int BRICK_GREEN_SPRITE4 = 30;
	public static final int BRICK_GREEN_SPRITE5 = 36;
	public static final int BRICK_GREEN_SPRITE6 = 42;
	public static final int BRICK_YELLOW_SPRITE1 = 13;
	public static final int BRICK_YELLOW_SPRITE2 = 19;
	public static final int BRICK_YELLOW_SPRITE3 = 25;
	public static final int BRICK_YELLOW_SPRITE4 = 31;
	public static final int BRICK_YELLOW_SPRITE5 = 37;
	public static final int BRICK_YELLOW_SPRITE6 = 43;
	public static final int ARROW_LEFT_SPRITE = 44;
	public static final int ARROW_RIGHT_SPRITE = 45;
	public static final int PARTICLE_FIRE = 100;
	public static final int PARTICLE_HALO = 101;
	public static final int PARTICLE_POINT = 102;
	public static final int PARTICLE_SQUARE = 103;
	
	private static TextureMng instance;
	
	private TextureMng() {
		// Cr�ation des ressources
		super.addTexture(new CVMTexture(PARTICLE_FIRE, "particles/particle_fire.png", 32, 32));
		super.addTexture(new CVMTexture(PARTICLE_HALO, "particles/particle_halo.png", 32, 32));
		super.addTexture(new CVMTexture(PARTICLE_POINT, "particles/particle_point.png", 32, 32));
		super.addTexture(new CVMTexture(PARTICLE_SQUARE, "particles/particle_square.png", 32, 32));
		super.addTexture(new CVMTexture(PADDLE_SPRITE, "sprite/paddle.png", 71, 28));
		super.addTexture(new CVMTexture(BALL_SPRITE, "sprite/ball.png", 336, 28, 12,1));
		super.addTexture(new CVMTexture(TITLE_SPRITE, "sprite/title.png", 526, 94));
		super.addTexture(new CVMTexture(PLAY_SPRITE, "sprite/title_play.png", 121, 54));
		super.addTexture(new CVMTexture(HIGHSCORE_SPRITE, "sprite/title_highscore.png", 256, 54));
		super.addTexture(new CVMTexture(PAUSE_SPRITE, "sprite/pause.png", 25, 30));
		super.addTexture(new CVMTexture(BACK_SPRITE, "sprite/title_back.png", 69, 31));
		super.addTexture(new CVMTexture(BRICK_ORANGE_SPRITE1, "sprite/bOrange1.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_ORANGE_SPRITE2, "sprite/bOrange2.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_ORANGE_SPRITE3, "sprite/bOrange3.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_ORANGE_SPRITE4, "sprite/bOrange4.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_ORANGE_SPRITE5, "sprite/bOrange5.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_ORANGE_SPRITE6, "sprite/bOrange6.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_PINK_SPRITE1, "sprite/bPink1.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_PINK_SPRITE2, "sprite/bPink2.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_PINK_SPRITE3, "sprite/bPink3.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_PINK_SPRITE4, "sprite/bPink4.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_PINK_SPRITE5, "sprite/bPink5.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_PINK_SPRITE6, "sprite/bPink6.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_RED_SPRITE1, "sprite/bRed1.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_RED_SPRITE2, "sprite/bRed2.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_RED_SPRITE3, "sprite/bRed3.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_RED_SPRITE4, "sprite/bRed4.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_RED_SPRITE5, "sprite/bRed5.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_RED_SPRITE6, "sprite/bRed6.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_BLUE_SPRITE1, "sprite/bBlue1.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_BLUE_SPRITE2, "sprite/bBlue2.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_BLUE_SPRITE3, "sprite/bBlue3.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_BLUE_SPRITE4, "sprite/bBlue4.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_BLUE_SPRITE5, "sprite/bBlue5.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_BLUE_SPRITE6, "sprite/bBlue6.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_GREEN_SPRITE1, "sprite/bGreen1.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_GREEN_SPRITE2, "sprite/bGreen2.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_GREEN_SPRITE3, "sprite/bGreen3.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_GREEN_SPRITE4, "sprite/bGreen4.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_GREEN_SPRITE5, "sprite/bGreen5.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_GREEN_SPRITE6, "sprite/bGreen6.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_YELLOW_SPRITE1, "sprite/bYellow1.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_YELLOW_SPRITE2, "sprite/bYellow2.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_YELLOW_SPRITE3, "sprite/bYellow3.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_YELLOW_SPRITE4, "sprite/bYellow4.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_YELLOW_SPRITE5, "sprite/bYellow5.png", 64, 64));
		super.addTexture(new CVMTexture(BRICK_YELLOW_SPRITE6, "sprite/bYellow6.png", 64, 64));
		super.addTexture(new CVMTexture(ARROW_LEFT_SPRITE, "sprite/arrow_left.png", 100, 50));
		super.addTexture(new CVMTexture(ARROW_RIGHT_SPRITE, "sprite/arrow_right.png", 100, 50));
	}
	
	public int getRandomBrickWithLvl(int level){
		Random rand = new Random();
		return rand.nextInt(6)+((level*6)+2);
	}
	
	public static TextureMng getInstance() {
		if (instance == null) {
			instance = new TextureMng();
		}
		
		return instance;
	}
}