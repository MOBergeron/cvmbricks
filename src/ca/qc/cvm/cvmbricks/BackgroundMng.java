package ca.qc.cvm.cvmbricks;

import java.util.Hashtable;
import java.util.Random;

import ca.qc.cvm.cvmbricks.BackgroundMng;

public class BackgroundMng {
	public static final int  CITY_NORMAL = 0;
	public static final int  CITY_CYAN   = 1;
	public static final int  CITY_GREEN  = 2;
	public static final int  CITY_PINK   = 3;
	public static final int  CITY_PURPLE = 4;
	public static final int  CITY_RED    = 5;
	public static final int  CITY_BLUE   = 6;
	
	private static BackgroundMng instance;
	
	private Hashtable<Integer, String> ht = new Hashtable<Integer, String>();
	
	private BackgroundMng() {
		// Cr�ation des ressources
		ht.put(CITY_NORMAL, "city_normal.png");
		ht.put(CITY_CYAN, "city_cyan.png");
		ht.put(CITY_GREEN, "city_green.png");
		ht.put(CITY_PINK, "city_pink.png");
		ht.put(CITY_PURPLE, "city_purple.png");
		ht.put(CITY_RED, "city_red.png");
		ht.put(CITY_BLUE, "city_blue.png");
	}
	
	public String getBackground(int number){
		return "background/" + ht.get(number);
	}
	
	public String getRandomBackground(){
		Random random = new Random();
		int rand = random.nextInt(7);
		return "background/" + ht.get(rand);
	}
	
	public static BackgroundMng getInstance() {
		if (instance == null) {
			instance = new BackgroundMng();
		}
		
		return instance;
	}
}