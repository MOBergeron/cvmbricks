package ca.qc.cvm.cvmbricks;

import ca.qc.cvm.cvmandengine.CVMSoundManager;
import ca.qc.cvm.cvmandengine.entity.CVMSound;

public class SoundMng extends CVMSoundManager {
	public static final int GG = 1;
	public static final int FAIL = 2;

	private static SoundMng instance;
	
	private SoundMng() {
		// Cr�ation du son explosion, qui est maintenant disponible via la m�thode : super.playSound(SoundMng.EXPLOSION)
		super.addSound(new CVMSound(GG, "sounds/gg.mp3"));
		super.addSound(new CVMSound(FAIL, "sounds/fail.mp3"));
	}
	
	public static SoundMng getInstance() {
		if (instance == null) {
			instance = new SoundMng();
		}
		
		return instance;
	}
}
