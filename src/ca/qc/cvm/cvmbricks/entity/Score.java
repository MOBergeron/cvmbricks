package ca.qc.cvm.cvmbricks.entity;

public class Score {
	private int id;
	private int level;
	private int score;

	public Score(int id, int level, int score) {
		this.id = id;
		this.level = level;
		this.score = score;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}

