package ca.qc.cvm.cvmbricks.entity;

public class Level {
	private int id;
	private int level;

	public Level(int id, int level) {
		this.id = id;
		this.level = level;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}

